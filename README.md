# 🚀 FastAPI + PostgreSQL (Docker Compose)

This project uses **FastAPI** and **PostgreSQL**, managed via `docker-compose`.

## 📦 Installation and Startup

1. **Clone the repository**:
   ```bash
   git clone git@gitlab.com:zeliksv/todo-api-python.git
   cd todo-api-python
   ```

2. **Create a `.env` file** (or use the example `.env.defaults`):
   ```bash
   cp .env.defaults .env
   ```

3. **Start the project with Docker Compose**:
   ```bash
   docker compose up --build
   ```

   🛠 To run in the background:
   ```bash
   docker compose up -d
   ```

4. **Check if the services are running**:
   - FastAPI Swagger UI: [http://localhost:8000/docs](http://localhost:8000/docs)
   - ReDoc: [http://localhost:8000/redoc](http://localhost:8000/redoc)
   - PostgreSQL: `localhost:5432`

## 🛑 Stopping the Containers
To stop and remove the containers:
```bash
   docker compose down
```

To remove containers and database data:
```bash
   docker compose down -v
```

## 🐳 Useful Commands
- View container logs:
  ```bash
  docker compose logs -f
  ```
- Start containers after stopping:
  ```bash
  docker compose start
  ```
- Stop containers without removing them:
  ```bash
  docker compose stop
  ```

## 🔹 Automatic `.env` Creation
This project supports **automatic `.env` generation** inside the container. 
If the `.env` file is missing, it will be created with a random password upon startup.

If you want to specify custom values, edit the `.env` file before starting the container.

---

### 🎯 Done! Your FastAPI is now running with PostgreSQL via Docker Compose 🚀
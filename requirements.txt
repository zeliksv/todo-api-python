fastapi==0.109.0
Faker==22.4.0
python-dotenv~=1.0.1
pydantic~=2.5.3
uvicorn~=0.14.0
psycopg2~=2.9.1
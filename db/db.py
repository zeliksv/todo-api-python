import psycopg2
from dotenv import load_dotenv
import os

load_dotenv()

db_params = {
    "host": os.getenv("DATABASE_HOST"),
    "user": os.getenv("DATABASE_USERNAME"),
    "port": os.getenv("DATABASE_PORT"),
    "password": os.getenv("DATABASE_PASSWORD"),
    "database": os.getenv("DATABASE_NAME")
}


class TodoDB:
    def __init__(self):
        self.conn = psycopg2.connect(**db_params)
        self.cursor = self.conn.cursor()

    def execute_query(self, query, values=None):
        self.cursor.execute(query, values)
        self.conn.commit()
        return self.cursor

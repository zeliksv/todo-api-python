import os
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from app.routes.todo import todo_router
from app.routes.users import users_router

app = FastAPI(
    title="todo on python",
    description="Description API for my todo",
    version="1.0.0"
)


# Configure CORS
origins = [os.getenv("CLIENT_URL")]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["GET", "POST", "PUT", "DELETE"],
    allow_headers=["Authorization", "Content-Type"],
)

app.include_router(todo_router)
app.include_router(users_router)

if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=8000)

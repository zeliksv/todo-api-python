#!/bin/bash

if [ ! -f .env ]; then
  echo "DATABASE_HOST=postgres" > .env
  echo "DATABASE_PORT=5432" >> .env
  echo "DATABASE_USERNAME=python_user" >> .env
  echo "DATABASE_PASSWORD=$(openssl rand -hex 12)" >> .env
  echo "DATABASE_NAME=python_api" >> .env
  echo "CLIENT_URL=http://localhost:4200" >> .env
  echo ".env file was made!"
fi

exec uvicorn main:app --host 0.0.0.0 --port 8000
import uuid

from app.helpers.parsers import parse_todo
from app.models.todo import TodoModel, CreateTodoModel, UpdateTodoModel, GetTodoBody
from app.services.queries.queries import GET_TODO, GET_ALL_TODO, UPDATE_TODO, DELETE_TODO, CREATE_TODO
from app.services.queries.user import UPDATE_USERS_TODOS
from db.db import TodoDB
from fastapi import HTTPException
from faker import Faker

fake = Faker()


class TodoService:
    @staticmethod
    def get_todo(todo_id: str):
        db = TodoDB()
        result = db.execute_query(GET_TODO, (todo_id,)).fetchone()

        if result:
            return TodoModel(**parse_todo(result))
        else:
            raise HTTPException(status_code=404, detail="Todo not found")

    @staticmethod
    def get_all_todos(user: GetTodoBody):
        db = TodoDB()
        try:
            results = db.execute_query(GET_ALL_TODO, (user.userId,)).fetchall()

            todos = []
            for result in results:
                if result:
                    todo = TodoModel(**parse_todo(result))

                    print(result)
                    print(todo)

                    todos.append(todo)
            return todos
        except Exception as e:
            raise HTTPException(status_code=500, detail=str(e))

    @staticmethod
    def create_todo(todo: CreateTodoModel):
        try:
            db = TodoDB()
            todo_id = str(uuid.uuid4())
            values = (todo.title, todo.description, todo_id, todo.userId)
            db.execute_query(CREATE_TODO, values)

            user_update_data = (todo_id, todo.userId)
            db.execute_query(UPDATE_USERS_TODOS, user_update_data)

            result = db.execute_query(GET_TODO, (todo_id,)).fetchone()

            if result:
                return TodoModel(**parse_todo(result))
        except Exception as e:
            raise HTTPException(status_code=500, detail=str(e))

    @staticmethod
    def update_todo(todo_id: str, todo: UpdateTodoModel):
        try:
            db = TodoDB()
            result = db.execute_query(GET_TODO, (todo_id,)).fetchone()

            updated_title = todo.title if todo.title is not None else result[1]
            updated_description = todo.description if todo.description is not None else result[2]

            values = (updated_title, updated_description, todo_id)

            db.execute_query(UPDATE_TODO, values)

            result = db.execute_query(GET_TODO, (todo_id,)).fetchone()

            if result:
                return TodoModel(**parse_todo(result))
        except Exception as e:
            raise HTTPException(status_code=500, detail=str(e))

    @staticmethod
    def create_fake_todos(user: GetTodoBody):
        db = TodoDB()
        try:
            for _ in range(10):
                todo_id = str(uuid.uuid4())

                values = (
                    fake.sentence(), fake.paragraph(), todo_id, user.userId)
                db.execute_query(CREATE_TODO, values)
                user_update_data = (todo_id, user.userId)
                db.execute_query(UPDATE_USERS_TODOS, user_update_data)
            return {"message": "10 fake todos created successfully"}
        except Exception as e:
            raise HTTPException(status_code=500, detail=str(e))

    @staticmethod
    def delete_todo(todo_id: str):
        db = TodoDB()
        db.execute_query(DELETE_TODO, (todo_id,))
        return {"message": "Todo deleted successfully"}

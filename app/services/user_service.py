from app.helpers.parsers import parse_user
from app.models.user import UserModel, CreateUserModel, UpdateUserModel, GetMeBodyRequest
from app.services.queries.user import GET_USER, CREATE_USER, GET_USER_BY_EMAIL, UPDATE_USER, DELETE_USER
from db.db import TodoDB
from fastapi import HTTPException


class UserService:
    @staticmethod
    def get_user(user_id: str):
        db = TodoDB()
        result = db.execute_query(GET_USER, (user_id,)).fetchone()

        if result:
            return UserModel(**parse_user(result))
        else:
            raise HTTPException(status_code=404, detail="User not found")

    @staticmethod
    def get_user_by_email(body: GetMeBodyRequest):
        db = TodoDB()
        result = db.execute_query(GET_USER_BY_EMAIL, (body.email,)).fetchone()

        if result:
            return UserModel(**parse_user(result))
        else:
            raise HTTPException(status_code=404, detail="User not found")

    @staticmethod
    def create_user(user: CreateUserModel):
        try:
            db = TodoDB()
            result = db.execute_query(GET_USER_BY_EMAIL, (user.email,)).fetchone()

            if result:
                raise HTTPException(status_code=404, detail=f"User with {user.email} already exists")
            else:
                values = (user.email, user.first_name, user.last_name, user.id, [], 0)
                db.execute_query(CREATE_USER, values)

            return UserModel(id=user.id, first_name=values[1], last_name=values[2], email=values[0], todos=[])
        except Exception as e:
            raise HTTPException(status_code=500, detail=str(e))

    @staticmethod
    def update_user(user_id: str, user: UpdateUserModel):
        try:
            db = TodoDB()

            values = {
                "user_id": user_id,
                "first_name": user.first_name,
                "last_name": user.last_name,
                "todos": user.todos
            }

            db.execute_query(UPDATE_USER, values)

            result = db.execute_query(GET_USER, (user_id,)).fetchone()

            if result:
                return UserModel(**parse_user(result))
        except Exception as e:
            raise HTTPException(status_code=500, detail=str(e))

    @staticmethod
    def delete_user(user_id: str):
        try:
            db = TodoDB()

            db.execute_query(DELETE_USER, (user_id,))

            return 'User was deleted success'
        except Exception as e:
            raise HTTPException(status_code=500, detail=str(e))

GET_TODO = 'SELECT * FROM todos WHERE id = %s'

GET_ALL_TODO = 'SELECT * FROM todos WHERE user_id = %s'

CREATE_TODO = 'INSERT INTO "todos" (title, description, id, user_id) VALUES (%s, %s, %s, %s)'

UPDATE_TODO = 'UPDATE todos SET title=%s, description=%s WHERE id=%s'

DELETE_TODO = 'DELETE FROM todos WHERE id=%s'


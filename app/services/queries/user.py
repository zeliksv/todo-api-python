GET_USER = 'SELECT * FROM "users" WHERE id = %s'

GET_USER_BY_EMAIL = 'SELECT * FROM "users" WHERE email = %s'

GET_ALL_USERS = 'SELECT * FROM users'

CREATE_USER = 'INSERT INTO "users" (email, first_name, last_name, id, todos, disabled) VALUES (%s, %s, %s, %s, %s, %s)'

UPDATE_USER = """UPDATE "users"
                      SET first_name = COALESCE(%(first_name)s, first_name),
                          last_name = COALESCE(%(last_name)s, last_name),
                          todos = COALESCE(%(todos)s, todos)
                      WHERE id = %(user_id)s;
"""

DELETE_USER = 'DELETE FROM "users" WHERE id=%s'

UPDATE_USERS_TODOS = 'UPDATE "users" SET todos = array_append(todos, %s) WHERE id = %s'

def parse_todo(result):
    todo_dict = {
        "id": result[0],
        "createdAt": result[1].strftime('%Y-%m-%d %H:%M:%S'),
        "title": result[2],
        "description": result[3],
        "userId": result[4]
    }

    return todo_dict


def parse_user(result):
    user_dict = {
        "id": result[0],
        "first_name": result[2],
        "last_name": result[3],
        "email": result[4],
        "todos": result[5],
        "disabled": bool(result[6])
    }

    return user_dict

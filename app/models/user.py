import uuid
from typing import Optional, List
from pydantic import BaseModel


class UserModel(BaseModel):
    id: str
    first_name: str
    last_name: str
    email: str
    todos: List[str]
    disabled: bool = False


class GetMeBodyRequest(BaseModel):
    email: str


class CreateUserModel(BaseModel):
    id: str = str(uuid.uuid4())
    first_name: str
    last_name: str
    email: str
    todos: List[str] = []


class UpdateUserModel(BaseModel):
    first_name: Optional[str] = None
    last_name: Optional[str] = None
    email: Optional[str] = None
    todos: List[str] = []
    disabled: bool = False

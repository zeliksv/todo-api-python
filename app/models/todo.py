import uuid
from typing import Optional
from pydantic import BaseModel


class TodoModel(BaseModel):
    id: str
    title: str
    description: str
    userId: str
    createdAt: str


class GetTodoBody(BaseModel):
    userId: str


class CreateTodoModel(BaseModel):
    id: str = str(uuid.uuid4())
    title: str
    description: str
    userId: str


class UpdateTodoModel(BaseModel):
    title: Optional[str] = None
    description: Optional[str] = None

from fastapi import APIRouter

from app.models.user import UserModel, CreateUserModel, UpdateUserModel, GetMeBodyRequest
from app.services.user_service import UserService

users_router = APIRouter()


@users_router.get("/user/{user_id}", response_model=UserModel)
async def read_user(user_id: str):
    return UserService.get_user(user_id)


@users_router.post("/user/me", response_model=UserModel)
async def read_user(body: GetMeBodyRequest):
    return UserService.get_user_by_email(body)


@users_router.post("/user/new", response_model=UserModel)
async def create_user(user: CreateUserModel):
    return UserService.create_user(user)


@users_router.put("/user/{user_id}", response_model=UserModel)
async def update_user(user_id: str, user: UpdateUserModel):
    return UserService.update_user(user_id, user)


@users_router.delete("/user/{user_id}", response_model=str)
async def delete_user(user_id: str):
    return UserService.delete_user(user_id)
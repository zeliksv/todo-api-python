from fastapi import APIRouter
from typing import List
from app.services.todo_service import TodoService
from app.models.todo import TodoModel, CreateTodoModel, UpdateTodoModel, GetTodoBody

todo_router = APIRouter()


@todo_router.get("/todos/{todo_id}", response_model=TodoModel)
async def read_todo(todo_id: str):
    return TodoService.get_todo(todo_id)


@todo_router.post("/todos-by-user", response_model=List[TodoModel])
async def read_todo(user: GetTodoBody):
    return TodoService.get_all_todos(user)


@todo_router.post("/todos/new", response_model=TodoModel)
async def create_todo(todo: CreateTodoModel):
    return TodoService.create_todo(todo)


@todo_router.put("/todos/{todo_id}", response_model=TodoModel)
async def update_todo(todo_id: str, todo: UpdateTodoModel):
    return TodoService.update_todo(todo_id, todo)


@todo_router.delete("/todos/{todo_id}", response_model=dict)
async def delete_todo(todo_id: str):
    return TodoService.delete_todo(todo_id)


@todo_router.post("/todos/create_fake")
async def create_fake_todos(user: GetTodoBody):
    TodoService.create_fake_todos(user)
    return {"message": "Endpoint for creating 10 fake todos triggered"}
